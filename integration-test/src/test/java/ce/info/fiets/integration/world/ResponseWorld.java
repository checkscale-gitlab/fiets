package ce.info.fiets.integration.world;

import io.cucumber.spring.ScenarioScope;
import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;

@ScenarioScope
public class ResponseWorld {
  private final List<Response> responses = new ArrayList<>();

  public void set(Response response) {
    this.responses.add(response);
  }

  public Response get() {
    return responses.get(responses.size() - 1);
  }

  public Response getPrevious() {
    return responses.get(responses.size() - 2);
  }
}
