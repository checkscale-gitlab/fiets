package ce.info.fiets.integration;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import ce.info.fiets.TestSetup;
import ce.info.fiets.integration.world.ResponseWorld;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.core.io.ClassPathResource;
import org.springframework.lang.NonNull;

public class TechnicalStepdefs {

  private final ResponseWorld responseWorld;

  public TechnicalStepdefs(@NonNull ResponseWorld responseWorld) {
    this.responseWorld = responseWorld;
  }

  @When("I get {string} from management")
  public void getFromManagement(String path) {
    Response response = given()
        .baseUri(TestSetup.getServiceUrl())
        .port(TestSetup.getActuatorPort())
        .when()
        .get(path)
        .then().extract().response();
    responseWorld.set(response);
  }

  @When("I get {string} from service")
  public void getFromService(String path) {
    responseWorld.set(
        given()
            .baseUri(TestSetup.getServiceUrl())
            .port(TestSetup.getServicePort())
            .when()
            .get(path)
            .then().extract().response());
  }

  @When("I post the text {string} to {string}")
  public void postTheText(String text, String apiUrl) {
    responseWorld.set(
        given()
            .baseUri(TestSetup.getServiceUrl())
            .port(TestSetup.getServicePort())
            .body(text)
            .when()
            .post(apiUrl)
            .then().extract().response());
  }

  @When("I post the content from {string} to {string}")
  public void postTheContentFromTo(String resourceName, String apiUrl) {
    String content = resourceAsString(resourceName);
    responseWorld.set(
        given()
            .baseUri(TestSetup.getServiceUrl())
            .port(TestSetup.getServicePort())
            .accept(ContentType.JSON)
            .contentType(ContentType.JSON)
            .body(content)
            .when()
            .post(apiUrl)
            .then().extract().response());
  }

  @Then("I receive a response with status {int}")
  @Then("the response has status code {int}")
  public void receiveResponseWithStatus(int expectedStats) {
    responseWorld.get().then()
        .statusCode(expectedStats);
  }

  @Then("the response body is equal to {string}")
  public void receiveResponseEqualTo(String expectedText) {
    responseWorld.get().then()
        .body(is(equalTo(expectedText)));
  }

  @Then("the response contains the text {string}")
  public void responseContainsText(String expectedText) {
    responseWorld.get().then()
        .body(containsString(expectedText));
  }

  @Then("the response body has the content of {string}")
  public void theResponseBodyHasTheContentOf(String resourcePath) {
    String expected = resourceAsString(resourcePath);
    responseWorld.get().then().assertThat()
        .body(is(equalToCompressingWhiteSpace(expected)));
  }


  @Then("the response body has the json content of {string}")
  public void theResponseBodyHasTheJsonContentOf(String resourcePath) throws JSONException {
    String expected = resourceAsString(resourcePath);
    JSONAssert.assertEquals(expected,
        responseWorld.get().then().extract().asString(), JSONCompareMode.LENIENT);
  }

  @Then("I can dump the response body")
  public void dumpTheResponseBody() {
    responseWorld.get().prettyPrint();
  }

  @Then("the content type is {word}")
  public void theContentTypeIs(String expectedContentType) {
    responseWorld.get().then()
        .contentType(containsString(expectedContentType));
  }

  @Then("the element {string} is equal to {string}")
  public void theElementIsEqualTo(String path, String expectedValue) {
    responseWorld.get().then()
        .assertThat()
        .body(path, is(equalTo(expectedValue)));
  }

  @Then("the element {string} is equal to {int}")
  public void theElementIsEqualTo(String path, int expectedValue) {
    responseWorld.get().then()
        .assertThat()
        .body(path, is(equalTo(expectedValue)));
  }

  @Then("the element {string} is not empty")
  public void theElementIsNotEmpty(String path) {
    responseWorld.get().then()
        .assertThat()
        .body(path, is(not(blankOrNullString())));
  }

  @Then("the element {string} contains {string}")
  public void theElementContains(String path, String expectedValue) {
    responseWorld.get().then()
        .assertThat()
        .body(path, containsString(expectedValue));
  }

  @Then("the element {string} has Item with {string}")
  public void theElementHasItemWith(String path, String expectedItem) {
    responseWorld.get().then()
        .assertThat()
        .body(path, hasItem(expectedItem));
  }

  private static String resourceAsString(String resourceName) {
    try {
      ClassPathResource classPathResource = new ClassPathResource(resourceName);
      try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
          classPathResource.getInputStream(), StandardCharsets.UTF_8))) {
        return bufferedReader.lines().collect(Collectors.joining(""));
      }
    } catch (IOException ioe) {
      throw new UncheckedIOException(ioe);
    }
  }

}
