package ce.info.fiets.controller;

import ce.info.fiets.domain.Article;

import com.rometools.rome.feed.atom.Content;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Feed;
import com.rometools.rome.feed.atom.Link;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.view.feed.AbstractAtomFeedView;

public class ArticleFeedView extends AbstractAtomFeedView {
  private final Article article;

  public ArticleFeedView(Article article) {
    super();
    this.article = article;
  }

  @Override
  protected void buildFeedMetadata(Map<String, Object> model,
                                   Feed feed,
                                   HttpServletRequest request) {
    feed.setTitle(article.getFeedTitle());
    feed.setModified(Date.from(article.getLastUpdate()));
  }

  @Override
  protected List<Entry> buildFeedEntries(Map<String, Object> map,
                                         HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse) {
    Entry entry = new Entry();

    Date articleUpdateTime = Date.from(article.getLastUpdate());
    entry.setUpdated(articleUpdateTime);
    entry.setCreated(articleUpdateTime);
    entry.setIssued(articleUpdateTime);
    entry.setModified(articleUpdateTime);

    entry.setTitle(article.getTitle());

    Link link = new Link();
    link.setHref(article.getContentLink());
    link.setTitle(article.getTitle());
    entry.setAlternateLinks(Collections.singletonList(link));

    Content content = new Content();
    content.setType(MediaType.TEXT_HTML_VALUE);
    content.setValue(article.getHtmlContent());
    entry.setContents(Collections.singletonList(content));

    entry.setId(article.getTitle() + articleUpdateTime.getTime());
    return Collections.singletonList(entry);
  }
}
