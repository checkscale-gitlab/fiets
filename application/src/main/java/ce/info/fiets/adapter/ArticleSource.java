package ce.info.fiets.adapter;

import ce.info.fiets.adapter.wikipedia.WikipediaRandomDownloader;
import ce.info.fiets.domain.Article;
import ce.info.fiets.domain.ArticleRepository;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Supplier;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class ArticleSource {

  @NonNull
  private final WikipediaRandomDownloader wikipediaRandomDownloader;

  @NonNull
  private final ArticleDownloader articleDownloader;

  @NonNull
  private final ArticleRepository articles;

  public Article randomWikipediaArticleFor(String language, String category, LocalTime updateTimeUtc, int depth) {
    String key = language + category + "_" + updateTimeUtc.format(DateTimeFormatter.ISO_LOCAL_TIME);
    Supplier<Article> articleSupplier =
        () -> wikipediaRandomDownloader.fetchRandomArticle(language, category, depth).setKey(key);

    return getAndCache(updateTimeUtc, key, articleSupplier);
  }

  public Article sameArticleFrom(String contentUrl, LocalTime updateTimeUtc, boolean allowScripting) {
    String key = contentUrl + "_" + updateTimeUtc.format(DateTimeFormatter.ISO_LOCAL_TIME);
    Supplier<Article> articleSupplier =
        () -> articleDownloader.fetchArticle(contentUrl, allowScripting).setKey(key);

    return getAndCache(updateTimeUtc, key, articleSupplier);
  }

  public Article randomLinkFrom(String url, String cssId, LocalTime updateTimeUtc) {
    String key = url + "_" + cssId + "_" + updateTimeUtc.format(DateTimeFormatter.ISO_LOCAL_TIME);
    Supplier<Article> articleSupplier =
        () -> articleDownloader.randomLinkFrom(url, cssId).setKey(key);

    return getAndCache(updateTimeUtc, key, articleSupplier);
  }

  private Article getAndCache(LocalTime updateTimeUtc, String key, Supplier<Article> articleSupplier) {
    log.info("Article requested for Key: {}", key);
    Article result = articles.findByKey(key)
        .map(storedArticle -> updateWhenExpired(storedArticle, updateTimeUtc, articleSupplier))
        .orElseGet(() -> articles.save(articleSupplier.get()));
    log.info("Article [{}] returned with lastUpdate={}",
        result.getTitle(),
        DateTimeFormatter.ISO_INSTANT.format(result.getLastUpdate()));
    return result;
  }

  private Article updateWhenExpired(Article storedArticle, LocalTime updateTimeUtc, Supplier<Article> articleSupplier) {
    ZonedDateTime nowUtc = ZonedDateTime.now(ZoneId.of("UTC"));
    if (storedArticle.isExpired(nowUtc, updateTimeUtc)) {
      Article newVersion = articleSupplier.get();
      storedArticle.setContentLink(newVersion.getContentLink());
      storedArticle.setFeedTitle(newVersion.getFeedTitle());
      storedArticle.setLastUpdate(newVersion.getLastUpdate());
      storedArticle.setTitle(newVersion.getTitle());
      storedArticle.setHtmlContent(newVersion.getHtmlContent());
      return articles.save(storedArticle);
    }
    return storedArticle;
  }

}
