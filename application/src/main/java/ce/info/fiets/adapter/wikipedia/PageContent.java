package ce.info.fiets.adapter.wikipedia;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PageContent {
  String title;
  String content;
}
