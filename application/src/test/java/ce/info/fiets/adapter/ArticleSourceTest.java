package ce.info.fiets.adapter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import ce.info.fiets.adapter.wikipedia.WikipediaRandomDownloader;
import ce.info.fiets.domain.Article;
import ce.info.fiets.domain.ArticleRepository;

import java.time.Instant;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ArticleSourceTest {

  private static final String LANGUAGE = "lang";
  private static final String CATEGORY = "Category";
  private static final String KEY = "Key";
  private static final LocalTime TIME_NEW_ARTICLE = LocalTime.of(15, 0);

  private static Article expected() {
    return Article.builder()
        .key(KEY)
        .feedTitle("feedTitle")
        .lastUpdate(Instant.now())
        .title("Title")
        .htmlContent("FeedContent")
        .contentLink("contentLink")
        .build();
  }


  private static Article expectedExpired() {
    return Article.builder()
        .key(KEY)
        .feedTitle("feedTitle2")
        .lastUpdate(Instant.now().minus(25, ChronoUnit.HOURS))
        .title("Title2")
        .htmlContent("FeedContent2")
        .contentLink("contentLink2")
        .build();
  }

  @Mock
  private WikipediaRandomDownloader wikipediaRandomDownloader;

  @Mock
  private ArticleDownloader articleDownloader;

  @Mock
  private ArticleRepository articleRepository;

  @InjectMocks
  private ArticleSource cut;

  @Test
  void whenCalledFirstThenReadNew() {
    given(articleRepository.findByKey("langCategory_15:00:00")).willReturn(Optional.empty());
    given(wikipediaRandomDownloader.fetchRandomArticle(LANGUAGE, CATEGORY, 4)).willReturn(expected());
    when(articleRepository.save(any())).thenAnswer(AdditionalAnswers.returnsFirstArg());

    Article article = cut.randomWikipediaArticleFor(LANGUAGE, CATEGORY, TIME_NEW_ARTICLE, 4);
    Article expectedArticle = expected().toBuilder().key("langCategory_15:00:00").build();
    assertThat(article).isEqualTo(expectedArticle);
    verify(articleRepository).save(expectedArticle);
  }

  @Nested
  class GivenValidArticleInStore {

    @BeforeEach
    void placeValidArticle() {
      given(articleRepository.findByKey("langCategory_15:00:00")).willReturn(Optional.of(expected()));
    }

    @Test
    void whenCalledAgainAndNotExpiredThenDoNotDownload() {

      Article article = cut.randomWikipediaArticleFor(LANGUAGE, CATEGORY, TIME_NEW_ARTICLE, 4);
      assertThat(article).isEqualTo(expected());
      verifyNoInteractions(wikipediaRandomDownloader);
      verify(articleRepository, never()).save(any());
    }
  }

  @Nested
  class GivenExpiredArticleInStore {

    @BeforeEach
    void placeExpiredArticle() {
      given(articleRepository.findByKey(anyString())).willReturn(Optional.of(expectedExpired()));
      when(articleRepository.save(any())).thenAnswer(AdditionalAnswers.returnsFirstArg());
    }

    @Test
    void whenCalledForWikipediaArticleThenDownloadNew() {
      given(wikipediaRandomDownloader.fetchRandomArticle(LANGUAGE, CATEGORY, 4)).willReturn(expected());

      Article article = cut.randomWikipediaArticleFor(LANGUAGE, CATEGORY, TIME_NEW_ARTICLE, 4);
      assertThat(article).isEqualTo(expected());
      verify(articleRepository).save(expected());
    }

    @Test
    void whenCalledForRandomLinkThenDownloadNew() {
      given(articleDownloader.randomLinkFrom("url", "cssid")).willReturn(expected());

      Article article = cut.randomLinkFrom("url", "cssid", TIME_NEW_ARTICLE);
      assertThat(article).isEqualTo(expected());
      verify(articleRepository).save(expected());
    }

    @Test
    void whenCalledForArticleThenDownloadNew() {
      given(articleDownloader.fetchArticle("url", true)).willReturn(expected());

      Article article = cut.sameArticleFrom("url", TIME_NEW_ARTICLE, true);
      assertThat(article).isEqualTo(expected());
      verify(articleRepository).save(expected());
    }
  }
}
